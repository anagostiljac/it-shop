

function onClick() {
    var emailInput = document.getElementById("email");
    var subjectInput = document.getElementById("subject");
    var emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if (subjectInput.value == "") {
        window.alert("Please enter your subject");
        subjectInput.focus();
        return false;
    }

  if (emailInput.value == "") {
       window.alert("Please enter your email");
       emailInput.focus();
       return false;
     
   }
   if(emailInput.value.match(emailRegex)){
       return true;
   }
   else {
       window.alert("Your email is not correct");
       emailInput.focus();
       return false;

   }
    
}